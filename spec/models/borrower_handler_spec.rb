require 'rails_helper'

RSpec.describe BorrowerHandler, type: :model do
  it {should belong_to(:user)}
  it {should belong_to(:book)}
end
