# README

API for Book Management System

This is the code regarding the API for a Book Management System


**Ruby version** 2.5.0

**PostgreSQL version** 11.5

**URL to use the API**
http://ekohe.smartstatistics.co 

**Postman collection**
The Postman Collection is found at the root of the project as 'Ekohe.postman_collection.json'

*Project Description*
The project has 3 models:
 - User
 - Book
 - Borrower_handler (funny name but does the work): Acts as the transaction having the fee, the relation between the user and the book, and finally the date of the book to be returned.

 The authentication is done by the auth controller using JWT. For the sake of the project, all endpoints are skipping this authentication but you can still create users and authenticate them

 The serialization is done using the gem ```active_model_serializers```

 There's a controller concern called ExceptionHandler which is responsible for rescuing the errors and giving the proper JSON response.

