Rails.application.routes.draw do
  resources :books, only: [:show]
  resources :users, only: [:show, :create] 
  resources :borrower_handler, only: []  do
    collection do
        post :borrow
        post :return
    end
  end
  resources :auth,  only: [] do
    collection do
        post :login
    end
  end

end
