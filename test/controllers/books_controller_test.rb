require 'test_helper'

class BooksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @book = books(:one)
  end

  test "should show book" do
    get book_url(@book), as: :json
    assert_response :success
  end

end
