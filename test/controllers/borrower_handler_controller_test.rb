require 'test_helper'

class BorrowerHandlerController < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  test "should borrow book" do
    assert_difference('User.count') do
      post users_url, params: { user: {  } }, as: :json
    end

    assert_response 201
  end

  test "should return book" do
    get user_url(@user), as: :json
    assert_response :success
  end

end
