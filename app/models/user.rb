class User < ApplicationRecord
    has_secure_password

    has_many :borrower_handlers

    has_many :active_lends, -> { active }, class_name: 'BorrowerHandler'
    has_many :books, through: :active_lends


    validates_uniqueness_of :email
    validates :balance, numericality: { greater_than_or_equal_to: 0 }

    def active_borrowed_books
         self&.borrower_handlers.where(active:true).includes(:book)&.map{|b| b.book}
    end

end
