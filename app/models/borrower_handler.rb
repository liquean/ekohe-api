class BorrowerHandler < ApplicationRecord
    belongs_to :user
    belongs_to :book

    scope :active, ->{where(active: true)}

    validate :book_availability, :balance_availability, on: :create

    before_save :set_return_date
    before_create :set_fee
    after_create :charge_fee
    after_save :update_book_availability

    def set_fee
        self.fee = BOOKING_FEE
    end

    def set_return_date
        self.return_date = Date.today + DEFAULT_DAYS_TO_RETURN.days
    end

    def update_book_availability
        self.book.quantity -= 1 if self.active?
        self.book.quantity += 1 if !self.active?
        self.book.save!
    end

    def charge_fee
        self.user.balance -= self.fee
        self.user.save!
    end


    private 
    
    def book_availability
        errors[:base] << 'No book available' if self.book.quantity < 1
    end

    def balance_availability
        errors[:base] << "You need to have at least #{BOOKING_FEE} credits in you balance in order to borrow this book" if self.user.balance < BOOKING_FEE
    end

end
