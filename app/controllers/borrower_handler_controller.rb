class BorrowerHandlerController < ApplicationController
  before_action :set_borrower_handler, only: [:return]
  before_action :check_params
  skip_before_action :authorize_request

  # POST /borrower_handler/borrow
  def borrow
    
    @borrower_handler = BorrowerHandler.new(borrower_handler_params)
    if @borrower_handler.save!
      render json: @borrower_handler, status: :created
    else
      render json: @borrower_handler.errors, status: :unprocessable_entity
    end
  end

  # POST /borrower_handler/return
  def return
    @borrower_handler.update!(active: false) 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def borrower_handler_params
      params.require(:borrower_handler).permit(:user_id, :book_id)
    end

    def set_borrower_handler
      @borrower_handler = BorrowerHandler.active.find_by(user_id:  params[:user_id], book_id: params[:book_id])
    end

    def check_params
      raise ArgumentError, "Book id is mandatory" if borrower_handler_params[:book_id].blank? 
      raise ArgumentError, "User id is mandatory"  if borrower_handler_params[:user_id].blank?
    end
end
