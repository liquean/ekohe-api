module ExceptionHandler
    extend ActiveSupport::Concern

    class AuthenticationError < StandardError; end

    included do
        rescue_from  ActiveRecord::RecordNotFound, with: :not_found
        rescue_from  ActiveRecord::RecordInvalid, with: :unprocessable_entity
        rescue_from  ExceptionHandler::AuthenticationError, with: :unauthorized
        rescue_from  ArgumentError, with: :unprocessable_entity
        rescue_from  NoMethodError, with: :unprocessable_entity
        rescue_from  BCrypt::Errors::InvalidHash, with: :unauthorized
        rescue_from  JWT::DecodeError, with: :not_found
    end

    private
    def not_found(e)
        json_response({message: e.message}, :not_found)
    end

    def unprocessable_entity(e)
        json_response({message: e.message}, :unprocessable_entity)
    end

    def unauthorized(e)
        json_response({message: e.message}, :unauthorized)
    end

    def json_response(object, status = :ok)
        render json: object, status: status
    end
end