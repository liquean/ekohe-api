class UsersController < ApplicationController
  before_action :set_user, only: [:show]
  skip_before_action :authorize_request, only: [:create, :show]

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    @user = User.new(create_user_params)
    if @user.save!
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:name, :last_name, :password)
    end

     # Only allow a trusted parameter "white list" through.
    def create_user_params
      params.require(:user).permit(:name, :last_name, :email, :password_digest, :balance)
    end
end
