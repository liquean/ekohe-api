class ApplicationController < ActionController::API
  before_action :authorize_request
  skip_before_action :authorize_request, only: :login
  include ExceptionHandler
  

  def authorize_request
    header = request.headers['Authorization']
    header = header.split(' ').last if header
    @decoded = JsonWebToken.decode(header)
    @current_user = User.find(@decoded[:user_id])
  end
end
