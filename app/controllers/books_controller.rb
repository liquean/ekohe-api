class BooksController < ApplicationController
  skip_before_action :authorize_request

  # GET /books/1
  def show
      start_date = Date.parse(params[:start_date]) unless params[:start_date].blank?
      end_date = Date.parse(params[:end_date]) unless params[:end_date].blank?
      raise ArgumentError, "Start date can not be greater than end date"  if !start_date.blank? && !end_date.blank? &&  start_date > end_date
      @book = Book.find params[:id] 
      borrower_handlers =  @book.borrower_handlers
      borrower_handlers =  borrower_handlers.where('borrower_handlers.created_at >= ?', start_date ) unless start_date.blank?
      borrower_handlers =  borrower_handlers.where('borrower_handlers.created_at <= ?', end_date) unless  end_date.blank?
      income = borrower_handlers.sum(:fee)
      render json: {book: @book.id, title: @book.title, income: income}
  end

end
