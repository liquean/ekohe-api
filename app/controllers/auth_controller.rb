class AuthController < ApplicationController
  skip_before_action :authorize_request, only: :login

  # POST /auth/login
  def login
    raise ExceptionHandler::AuthenticationError, "Must send an email"  unless !params[:email].blank?
    raise ExceptionHandler::AuthenticationError, "Must send a password"  unless !params[:password].blank?
    @user = User.find_by_email(params[:email])
    
    unless @user.blank? || params[:password].blank? || !@user&.authenticate(params[:password])
      token = JsonWebToken.encode(user_id: @user.id)
      render json: { token: token }, status: :ok
    else
      raise ExceptionHandler::AuthenticationError, "Email or password are wrong" 
    end

  end

  private

  def login_params
    params.permit(:email, :password)
  end
end
