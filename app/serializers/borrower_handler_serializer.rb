class BorrowerHandlerSerializer < ActiveModel::Serializer
  attributes :return_date, :fee, :book

  def book
    BookSerializer.new(object.book, { root: false } )
  end
end
