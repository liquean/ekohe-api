class UserSerializer < ActiveModel::Serializer
  attributes :id, :balance
  has_many :active_lends, serializer: BorrowerHandlerSerializer

end
