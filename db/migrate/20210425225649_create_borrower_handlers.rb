class CreateBorrowerHandlers < ActiveRecord::Migration[5.1]
  def change
    create_table :borrower_handlers do |t|
      t.decimal :fee
      t.datetime :return_date,  null: false
      t.boolean :active, default: true
      t.references :user, null: false, foreign_key: true
      t.references :book, null: false, foreign_key: true
      t.timestamps
    end
  end
end
